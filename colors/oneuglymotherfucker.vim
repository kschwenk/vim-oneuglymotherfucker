" Scheme setup {{{
hi! clear

if exists("syntax_on")
  syntax reset
endif

let colors_name="oneuglymotherfucker"

" }}}
" User Interface {{{
" Terminal cursor is done by terminal...
hi Cursor guifg=White guibg=DarkGreen gui=NONE
" CursorLine defaults to underline, which I like better than reverse.
" Cursorcolumn doesn't look so good with it however. Still, I link both
hi! link CursorColumn CursorLine
hi Visual          ctermfg=White     ctermbg=DarkBlue               guifg=White     guibg=DarkBlue
hi StatusLine      ctermfg=White     ctermbg=DarkBlue cterm=bold    guifg=White     guibg=DarkBlue gui=bold
hi StatusLineNC    ctermfg=LightGray ctermbg=DarkGray cterm=NONE    guifg=LightGray guibg=DarkGray gui=NONE
hi!link VertSplit StatusLineNC
hi LineNr          ctermfg=DarkGray  ctermbg=NONE                   guifg=DarkGray  guibg=NONE
hi CursorLineNr    ctermfg=NONE      ctermbg=DarkGray cterm=bold    guifg=NONE      guibg=DarkGray
hi! link SignColumn LineNr

" I use ColorColumn to indicate the line length limit in some filetypes,
" overflowing chars appear like comments
hi! link ColorColumn Comment
hi! link NonText Comment

" hi ErrorMsg      ctermfg=LightGray ctermbg=Red
" hi WarningMsg    ctermfg=LightGray ctermbg=Yellow

hi DiffAdd       ctermfg=White ctermbg=DarkGreen
hi DiffChange    ctermfg=White ctermbg=DarkYellow
hi DiffDelete    ctermfg=White ctermbg=DarkRed
hi DiffText      ctermfg=White ctermbg=DarkMagenta

hi Pmenu         ctermfg=LightGray ctermbg=DarkGray     guifg=LightGray guibg=DarkGray
hi PmenuSel      ctermfg=White     ctermbg=DarkBlue     guifg=White     guibg=DarkBlue
hi PmenuSBar                       ctermbg=DarkGray                     guibg=DarkGray
hi PmenuThumb                      ctermbg=LightGray                    guibg=LightGray

" }}}
" Generic syntax {{{
hi MatchParen    term=underline ctermfg=NONE ctermbg=NONE cterm=underline    guifg=NONE guibg=NONE gui=underline
hi Comment                      ctermfg=DarkGray                             guifg=DarkGray
hi Constant                     ctermfg=Blue
hi Special                      ctermfg=DarkYellow
hi! link PreProc Special

" }}}
" Diff {{{
hi diffRemoved ctermfg=DarkRed     ctermbg=NONE
hi diffAdded   ctermfg=DarkGreen   ctermbg=NONE
hi diffChanged ctermfg=DarkYellow  ctermbg=NONE

" }}}
" GitGutter {{{
hi! link GitGutterAdd diffAdded
hi! link GitGutterChange diffChanged
hi! link GitGutterDelete diffRemoved

" }}}
" Help {{{
hi! link helpExample String

" }}}
" Whitespace {{{
hi! link ExtraWhitespace WarningMsg

" }}}
