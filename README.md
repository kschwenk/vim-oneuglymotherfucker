My private, slight modification of vim's default colorscheme

Features:
  - Tuned down line numbers
  - Tuned down PMenu
  - Tuned down comments
  - Clear statusline for active/inactive windows
  - Clear visual selection
  - Unclear ColorColumn (links to Comment, you'll only see it when you *write* into colorcolumn, I use this as a tuned down max line length indicator)
  - More intuitive diff colors

Design guidelines:
  - Should look bearable in terminal (including 2, 8, and 16 color terminals)
  - Should look bearable in gui
  - Should look bearable with light and dark background (adjust Normal highlighting group and background option in vimrc)
  - Should take the edge off the default scheme

Rationale:
I don't use vim for much these days, but when I use it, I want syntax highlighting.
Specifically, I want few, high-contrast colors, and I want the colorscheme to degrade gracefully.
Vim's default scheme is pretty good at this, but I find some aspects distractingly ugly.
This colorscheme contains my private mitigations, so I can edit files in vim every now and then.

This is public so plugin managers can pull it without credentials.
